<h1> Readme - Available NPM Scripts </h1>
    <i><b><bigger> Typing npm commands into the console will only work if you have installed nodejs. </bigger></b></i>(
    <a href="https://nodejs.org/en/download/"> nodejs.org/en/download/ </a> )

<section style="padding-top: 60px">
    <h2><b> Command List </b></h2>
    <table style="width:100%">
        <tr>
            <th>Command</th>
            <th>Description</th>
        </tr>
            <tr>
            <td><b><bigger>npm install</bigger></b></td>
            <td> Installs dependencies required by this project </td>
        </tr>
        <tr>
            <td><b><bigger>npm start</bigger></b></td>
            <td> Starts a development server for this project. </td>
        </tr>
        <tr>
            <td><b><bigger>npm run build</bigger></b></td>
            <td> Creates a production build for this project. </td>
        </tr>
    </table>
    <br>
    <i>
        To start the development server, its adviced to run <b>'npm install'</b> and after that <b>'npm start'</b>, to keep the dependencies up-to-date.
    </i>
</section>




<head>
    <link rel="stylesheet" href="./docs/docs.css">
</head>
<article> 
    <header> 
        <h1> Boilerplate Docs - Landing Page </h1>
        <p> 
            Welcome to my Webpack Boilerplate.
            This Boilerplate allows to easily and quickly create new Javascript projects. 
        </p>
    </header>
    <main>
        <section>
            <h2> Feature List </h2>
            <ul>
                <li> Development Server </li>
                <li> ES Modules </li>
                <li> ES6 Syntax </li>
                <li> Sass Stylesheets </li>
                <li> NPM Packages </li>
                <li> Production Builds </li>
            </ul>
        </section>
        <section>
            <h3> Try another Edition of the Webpack Boilerplate ! </h3>
            <p>
                This repository has 6 production ready branches, reaching from Vanilla to Typescript with React and Storybook.
            </p>
        </section>
    </main>
    <hr>
    <footer>
        <h3> Documentation Overview </h3>
        <p> 
            <a class="bold" href="./docs/readme.md"> Click here </a> to get to the Documentation Overview.
        </p>
    </footer>
</article>
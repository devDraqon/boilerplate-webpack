<head>
    <link rel="stylesheet" href="./docs.css">
</head>
<article> 
    <header> 
        <h1> Boilerplate Docs - Git </h1>
        <section>
            <h3> What is Git? </h3>
            <p> 
                Git is the most commonly used version control system. It tracks the changes you and your colleagues make to files, and maintains every commited set of changes as a new version.
            </p>
        </section>
    </header>
    <aside>
        <h3> Requirements </h3>
        <p> git must be installed locally on your machine.
        </p>
    </aside>
    <main>
        <section>
            <table>
                <h3> clone and change branch </h3>
                <tr>
                    <th> Command </th>
                    <th> Description </th>
                </tr>
                <tr>
                    <td class="bold"> git clone https://url_to_repo.git </td>
                    <td> Clone repository into current terminal path. </td>
                </tr>
                <tr>
                    <td class="bold"> git checkout branchname </td>
                    <td> Leave this branch and go to another branch. </td>
                </tr>
            </table>
        </section>
        <section>
            <table>
                <h3> get branch information </h3>
                <tr>
                    <th> Command </th>
                    <th> Description </th>
                </tr>
                <tr>
                    <td class="bold"> git branch </td>
                    <td> Show all branches of this repository. (highlights current branch) </td>
                </tr>
                <tr>
                    <td class="bold"> git status </td>
                    <td> Show changed files since last commit. (marks untracked files red & tracked files green) </td>
                </tr>
                <tr>
                    <td class="bold"> git log </td>
                    <td> Show history of previous commits of this branch. </td>
                </tr>
            </table>
        </section>
        <section>
            <table>
                <h3> track and untrack files </h3>
                <tr>
                    <th> Command </th>
                    <th> Description </th>
                </tr>
                <tr>
                    <td class="bold"> git add filename </td>
                    <td> Track changes in file or directory.</td>
                </tr>
                <tr>
                    <td class="bold"> git rm --cached filename </td>
                    <td> Untrack changes in file or directory. </td>
                </tr>
            </table>
        </section>
        <section>
            <table>
                <h3> create and handle local commits </h3>
                <tr>
                    <th> Command </th>
                    <th> Description </th>
                </tr>
                <tr>
                    <td class="bold"> git stash </td>
                    <td> Reset changes to last commit. </td>
                </tr>
                <tr>
                    <td class="bold"> git commit -m "your commit message" </td>
                    <td> Bundle all tracked files into a commit with a message. </td>
                </tr>
                <tr>
                    <td class="bold"> git reset --soft HEAD~1 </td>
                    <td> Undo last local commit. </td>
                </tr>
            </table>
        </section>
        <section>
            <table>
                <h3> exchange data with online repo </h3>
                <tr>
                    <th> Command </th>
                    <th> Description </th> 
                </tr>
                <tr>
                    <td class="bold"> git fetch </td>
                    <td> Fetch global changes from online repository. </td>
                </tr>
                <tr>
                    <td class="bold"> git pull </td>
                    <td> Pull changes from this brach from online repository. </td>
                </tr>
                <tr>
                    <td class="bold"> git push </td>
                    <td> Push changes from this brach to online repository. </td>
                </tr>
            </table>
        </section>
    </main>
    <aside>
        <h3> Learn more about Git </h3>
        <a class="bold" href="https://git-scm.com/about"> Click here </a> to visit Gits Website.
    </aside>
    <hr>
    <footer>
        <h3> Documentation Overview </h3>
        <p> 
            <a class="bold" href="./readme.md"> Click here </a> to get back to the Documentation Overview.
        </p>
    </footer>
</article>

<h1> Readme - Using Git Repositories </h1>
    <i><b><bigger> Typing git commands into the console will only work if you have installed git. </bigger></b></i>(
    <a href="https://git-scm.com/downloads"> git-scm.com/downloads</a> )

<section style="padding-top: 60px">
    <h2><b> Command List </b></h2>
    <table style="width:100%">
        <tr>
            <th>Command</th>
            <th>Description</th>
        </tr>
        <tr>
            <td><b><bigger>git clone https://gitlab.com/username/reponame.git</bigger></b></td>
            <td> Clone repository into current terminal path </td>
        </tr>
        <tr>
            <td><b><bigger>git branch</bigger></b></td>
            <td> List all branches of this repository (highlights current branch) </td>
        </tr>
        <tr>
            <td><b><bigger>git checkout branchname</bigger></b></td>
            <td> Leave this branch and go to another branch </td>
        </tr>
        <tr>
            <td><b><bigger>git fetch</bigger></b></td>
            <td> Fetch global changes from online repository </td>
        </tr>
        <tr>
            <td><b><bigger>git pull</bigger></b></td>
            <td> Pull changes from this brach from online repository </td>
        </tr>
    </table>
</section>


<section style="padding-top: 60px">
<h2><b> Clone Repository </b></h2>
<p> Open the terminal and navigate to a valid parent folder for this repository. Alternatively you can open your terminal / command line and paste this to get to your documents folder.
</p>

    cd %userprofile%/documents

<p> To actually clone the repository, you can use the following command. A folder for this repository will then be cloned into the folder your terminal is currently in
</p>

    git clone https://gitlab.com/devDraqon/boilerplate-webpack.git

</section>


<section style="padding-top: 60px">
    <h2><b> Change Branch </b></h2>
    <p>
        Following commands will <b>fetch</b> all changes from the original repo, list all <b>branch</b>es, <b>checkout</b> into the Vanilla branch and <b>pull</b> all the changes of the current branch.
    </p>

        git fetch   
        git branch
        git checkout Vanilla
        git pull

</section>


<section style="padding-top: 60px">
    <h2><b> Issues </b></h2>
    <p>
       If you encounter any problem, please create an Issue to describe it by following this link:
       <br>
       <a href="https://gitlab.com/devDraqon/boilerplate-webpack/-/issues/new"> gitlab.com/devDraqon/boilerplate-webpack/-/issues/new </a>
    </p>

</section>
